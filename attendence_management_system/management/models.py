from django.db import models
from django.contrib.auth.models import User
import datetime

# Create your models here.
privileges = (
    ('S', 'Student'),
    ('T', 'Teacher'),
)

class User_Privilege(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True)
    privilege = models.CharField(max_length=5, choices=privileges)
    updated = models.DateTimeField(auto_now=True)
    created = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.user.username

class Course(models.Model):
    course_code = models.CharField(max_length=10, primary_key = True)
    course_title = models.CharField(max_length=70)
    year = models.PositiveIntegerField()
    def __str__(self):
        return self.course_title
    class Meta():
        ordering = ['year']

class Attendence(models.Model):
    date = models.DateField()
    course_code = models.ForeignKey(Course, on_delete=models.SET_NULL, null=True)
    student_id = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    def __str__(self):
        return self.course_code

class CourseUndertake(models.Model):
    course = models.ForeignKey(Course, on_delete=models.SET_NULL, null=True)
    teacher = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    def __str__(self):
        return f'{self.course} -- {self.teacher}'


