from django import forms

class LoginForm(forms.Form):
    username = forms.CharField(widget= forms.TextInput(attrs={'name': 'username', 'id': 'username'}))
    password = forms.CharField(widget= forms.PasswordInput(attrs={'name': 'password', 'id': 'password'}))

class CompleteProfile(forms.Form):
    privileges = (
    ('S', 'Student'),
    ('T', 'Teacher'),
)
    firstname = forms.CharField(widget= forms.TextInput(attrs={'name' : 'First name'}))
    lastname = forms.CharField(widget= forms.TextInput(attrs= {'name' : 'Last name'}))
    email = forms.EmailField(widget= forms.EmailInput(attrs= {'name' : 'Email address'}))
    role = forms.ChoiceField(choices= privileges, required= True)