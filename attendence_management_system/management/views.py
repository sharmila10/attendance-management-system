from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout
from .forms import LoginForm, CompleteProfile
from django.contrib.auth.models import User
from .models import User_Privilege

# Create your views here.
def signinForm(request):
    form = LoginForm
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect('/')
        else:
            return render(request, 'management/signin.html', {'form': form, 'error': 'Wrong Username/Password'})
    else:
        return render(request, 'management/signin.html', {'form': form})

def registrationForm(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            user = authenticate(request, username= request.POST['username'], password=request.POST['password1'])
            if user is not None:
                login(request, user)
                return redirect('/profile')
            else:
                return redirect('/')
    else:
            form = UserCreationForm()
    return render(request, 'management/registration.html', {'form': form})

def profile(request):
    if request.method == 'POST':
        if request.user.is_authenticated:
            username = request.user.username
        firstname = request.POST['firstname'][0]
        lastname = request.POST['lastname'][0]
        email = request.POST['email'][0]
        role = request.POST['role'][0]
        userDB = User.objects.get(username=username)
        userDB.first_name = firstname
        userDB.last_name = lastname
        userDB.email = email
        userDB.save()
        del userDB
        user_priDB = User_Privilege()
        user_priDB.user = User.objects.get(username=username)
        user_priDB.privilege = role
        user_priDB.save()
        return redirect('/') ## Temp
    else:
        form = CompleteProfile
        return render(request, 'management/profile.html', {'form': form})

def signout(request):
    if request.method == 'POST':
        pass
    logout(request)
    return redirect('/')
