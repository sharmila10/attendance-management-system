from django.urls import path
from . import views

app_name = 'management'

urlpatterns = [
    path('', views.signinForm, name='login'),
    path('registration/', views.registrationForm, name='signup'),
    path('profile/', views.profile, name='profile'),
    path('logout/',views.signout, name='logout'),
]