from django.contrib import admin
from .models import User_Privilege, Course, Attendence

# Register your models here.
admin.site.register(User_Privilege)
admin.site.register(Course)
admin.site.register(Attendence)
